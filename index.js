const puppeteer = require("puppeteer");
const { writeText } = require("./utils");

(async () => {
  const browser = await puppeteer.launch({
    headless: false,
  });
  const page = await browser.newPage();
  await page.goto("https://www.lemonde.fr");

  // Les méthodes de page renvoient un objet Pupeteer qui devra être évalué avec page.evaluate

  // Get one node from DOM : await page.$('CSS selector')
  // Return ElementHandle or null
  // Get various nodes from DOM : await page.$$('CSS selector')
  // Return ElementHandle[]

  // get textContent from one ElementHandle : const data = await page.evaluate(el => el.textContent, ElementHandle);
  // marche avec d'autres attributs (dont href pour les balises <a>)

  await page.waitForTimeout(5000); // Permet de faire patienter le navigateur X milliseconds
  await page.waitForSelector("body"); // Prends un selecteur CSS en paramètre - attends que le selecteur CSS apparaisse dans le DOM, jusqu'à 30 secondes avant de planter

  // JSON.stringify
  await writeText("voilà du beau json");

  await browser.close();
})();
