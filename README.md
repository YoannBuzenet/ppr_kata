## On va scrapper le monde !

Le monde ? **lemonde.fr** en tous cas, oui !<br/>
Notre mission : créer un fichier `.json` avec les infos des articles de la page principale<br/>

Le json doit contenir, pour chaque article

- Son titre
- L'url de l'article
- L'url de la photo

(voir [cette image](assets/illustrations.png) pour exemple)

L'output final désiré est un fichier `.json` contenant un array d'objets, chaque objet étant un article. Minimum 20 objets.

Les méthodes de Pupeteer à utiliser sont indiquées dans `index.js`
